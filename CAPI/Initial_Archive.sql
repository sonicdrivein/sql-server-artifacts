USE [COMMON_WEB_V2]
GO


DECLARE @Days int
SET @Days = -100

WHILE(@Days < -32)
BEGIN

EXECUTE [dbo].[sp_Archvie_ApiLog] 
   @Days

SET @Days = @Days + 1

END
GO

--Find start date
SELECT [ID] 
  FROM [COMMON_WEB_V2].[dbo].[ApiLog] (NOLOCK)
  WHERE [RequestDate] < DATEADD(DD,-396,GETDATE())

--6 days 12 seconds
--100 days 133 seconds
