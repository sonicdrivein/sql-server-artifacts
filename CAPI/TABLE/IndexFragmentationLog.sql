/****** Object:  Table [dbo].[IndexFragmentationLog]    Script Date: 4/9/2018 5:21:41 PM ******/
USE [SonicAPI]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[IndexFragmentationLog](
	[Completed_Date] [datetime] NOT NULL,
	[Database] [nvarchar](128) NULL,
	[Table] [sysname] NOT NULL,
	[Index] [sysname] NULL,
	[avg_fragmentation_in_percent] [float] NULL,
	[page_count] [bigint] NULL
) ON [PRIMARY]

GO


CREATE UNIQUE CLUSTERED INDEX [PK_IndexFragmentationLog] ON [dbo].[IndexFragmentationLog]
(
	[Completed_Date] ASC,
	[Database] ASC,
	[Table] ASC,
	[Index] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
