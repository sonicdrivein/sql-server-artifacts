USE [COMMON_WEB_V2]
GO

/****** Object:  Table [dbo].[ApiLogArchive]    Script Date: 4/9/2018 1:12:13 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING OFF
GO

CREATE TABLE [dbo].[ApiLogArchive](
	[ID] [int] NOT NULL,
	[RequestDate] [datetime] NULL,
	[APIAccountID] [int] NULL,
	[IPAddress] [varchar](50) NULL,
	[RequestURI] [varchar](255) NULL,
	[RequestData] [varchar](max) NULL,
	[ErrorData] [varchar](max) NULL,
	[SuccessDate] [datetime] NULL,
	[ErrorDate] [datetime] NULL,
	[Signature] [varchar](50) NULL,
	[ExecutionTime] [int] NULL,
	[ParentID] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
SET ANSI_PADDING ON
ALTER TABLE [dbo].[ApiLogArchive] ADD [UserAgent] [varchar](max) NULL
 CONSTRAINT [PK_ApiLogArchive] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


