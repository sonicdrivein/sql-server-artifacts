USE [COMMON_WEB_V2]
GO

/****** Object:  StoredProcedure [dbo].[sp_Archvie_ApiLog]    Script Date: 5/11/2018 1:14:53 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		Matt Leming
-- Create date: 04/10/2018
-- Description:	Archives any data older than @Days days but no sooner than 30
-- =============================================

CREATE PROCEDURE [dbo].[sp_Archvie_ApiLog] (@Days int)
AS
BEGIN
  DECLARE @TempApiLog table ([ID] int NOT NULL);
  DECLARE @Counter int
  DECLARE @Max int
  
  IF (@Days > -31)
	BEGIN
		SET @Days = -31
	END  

  INSERT INTO @TempApiLog
  SELECT [ID] 
  FROM [COMMON_WEB_V2].[dbo].[ApiLog] (NOLOCK)
  WHERE [RequestDate] < DATEADD(DD,@Days,GETDATE())

  SELECT @Max = MAX([ID])
  FROM @TempApiLog

  SELECT @Counter = MIN([ID])
  FROM @TempApiLog

  WHILE (@Counter <= @Max)
  BEGIN

  DELETE AL
  OUTPUT deleted.* INTO  [COMMON_WEB_V2].[dbo].[ApiLogArchive]
  FROM [COMMON_WEB_V2].[dbo].[ApiLog] AS AL
	INNER JOIN @TempApiLog T
		ON AL.[ID] = T.[ID]
		AND T.[ID] BETWEEN @Counter AND (@Counter + 1000)

  SELECT @Counter = @Counter + 1000   

  END
END


GO


