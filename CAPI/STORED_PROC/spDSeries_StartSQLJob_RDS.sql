USE [SonicAPI]
GO

/****** Object:  StoredProcedure [dbo].[spDSeries_StartSQLJob_RDS]    Script Date: 5/2/2018 11:10:18 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




/*==========================================================================================
TITLE:		spDSeries_StartSQLJob_RDS

DATE:		06/15/2009

AUTHOR:  	M. Leming

DESCRIPTION:  Runs a SQL Agent Job
				
	
NOTES: Used so Dseries will call a job and not move on until the job has finished

TABLES/VIEWS: 

STORED PROCEDURES:	

REVISION HISTORY
	Author			Date modified		Description of Changes
	Matt Leming		08/11/2016			Added @WaitCycleTime as an optional parameter
	Matt Leming		05/02/2018			Modified from original to work in RDS

===========================================================================================*/
CREATE PROCEDURE [dbo].[spDSeries_StartSQLJob_RDS]
	@Job_Name varchar(max),
	@Step_Number int,
	@WaitCycleTime varchar(8) = '00:01:00'
AS

DECLARE @Step_Name varchar(max)
DECLARE @ErrorCode AS int
DECLARE @Running AS int
DECLARE @Success AS int 
DECLARE @SQLJob_ID AS varchar(max)


SET @ErrorCode = 9999
SET @Running = 1
	
BEGIN TRY 
	SELECT @SQLJob_ID = SJ.Job_ID
	FROM msdb.dbo.SysJobs SJ
	WHERE SJ.[NAME] = @Job_Name


	EXEC msdb.dbo.sp_start_job @Job_Name = @Job_Name

	WHILE(@Running >= 1)
	BEGIN
		-- Waits specified about of time
		WaitFor Delay @WaitCycleTime

		SELECT @Running = COUNT(*)
		FROM msdb.dbo.sysjobactivity JA
		WHERE JA.start_execution_date is not null
			AND JA.stop_execution_date is null
			AND JA.Job_ID = @SQLJob_ID

		SELECT @Success = HIS.Run_Status
		FROM [msdb].[dbo].[sysjobhistory] HIS
			INNER JOIN 
			(
				SELECT MAX(Instance_ID) as Instance_ID
				FROM [msdb].[dbo].[sysjobhistory]
				WHERE Job_ID = @SQLJob_ID
					AND Step_ID = 0
			) HIS2
				ON HIS.Instance_ID = HIS2.Instance_ID

	END
END TRY
BEGIN CATCH
	SET @ErrorCode = 7027
END CATCH

IF @ErrorCode = 9999 AND @Success = 1
	BEGIN
		SELECT 0
	END
ELSE 
	BEGIN
		SELECT 1
	END 







GO


