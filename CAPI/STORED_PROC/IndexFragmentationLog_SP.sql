USE [COMMON_WEB_V2]
GO

INSERT INTO [SonicAPI].[dbo].[IndexFragmentationLog]
SELECT DISTINCT 
GETDATE() AS Completed_Date,
DB_NAME(DB_ID()) as [Database],
dbtables.[name] as 'Table', 
dbindexes.[name] as 'Index',
indexstats.avg_fragmentation_in_percent,
indexstats.page_count
FROM sys.dm_db_index_physical_stats (DB_ID(), NULL, NULL, NULL, NULL) AS indexstats
INNER JOIN sys.tables dbtables 
	ON dbtables.[object_id] = indexstats.[object_id]
	AND indexstats.fragment_count IS NOT NULL
INNER JOIN sys.indexes AS dbindexes 
	ON dbindexes.[object_id] = indexstats.[object_id]
	AND dbindexes.index_id = indexstats.index_id

GO

USE [COMMON_PERMISSIONS_V2]
GO

INSERT INTO [SonicAPI].[dbo].[IndexFragmentationLog]
SELECT DISTINCT 
GETDATE() AS Completed_Date,
DB_NAME(DB_ID()) as [Database],
dbtables.[name] as 'Table', 
dbindexes.[name] as 'Index',
indexstats.avg_fragmentation_in_percent,
indexstats.page_count
FROM sys.dm_db_index_physical_stats (DB_ID(), NULL, NULL, NULL, NULL) AS indexstats
INNER JOIN sys.tables dbtables 
	ON dbtables.[object_id] = indexstats.[object_id]
	AND indexstats.fragment_count IS NOT NULL
INNER JOIN sys.indexes AS dbindexes 
	ON dbindexes.[object_id] = indexstats.[object_id]
	AND dbindexes.index_id = indexstats.index_id

GO

