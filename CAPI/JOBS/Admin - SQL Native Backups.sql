USE [msdb]
GO

/****** Object:  Job [Admin - SQL Native Backups]    Script Date: 11/5/2018 11:10:42 AM ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [Database Maintenance]    Script Date: 11/5/2018 11:10:42 AM ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'Database Maintenance' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'Database Maintenance'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'Admin - SQL Native Backups', 
		@enabled=1, 
		@notify_level_eventlog=0, 
		@notify_level_email=0, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'No description available.', 
		@category_name=N'Database Maintenance', 
		@owner_login_name=N'sonicadmin', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Back up databases]    Script Date: 11/5/2018 11:10:42 AM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Back up databases', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'DECLARE @backupdate varchar(8),@SQL varchar(8000)

SET @backupdate = rtrim(ltrim(cast(convert(char(11), month(getdate())) as varchar(2)))) + rtrim(ltrim(cast(convert(char(11), day(getdate())) as varchar(2)))) + rtrim(ltrim(cast(convert(char(11), year(getdate())) as varchar(4))))

SET @SQL = ''exec msdb.dbo.rds_backup_database @source_db_name=''''COMMON_PERMISSIONS_V2'''','' + ''@s3_arn_to_backup_to='' + '''''''' + ''arn:aws:s3:::capi-db-prod-backup.sonicdrivein.com/COMMON_PERMISSIONS_V2_'' + @backupdate + ''.bak'' + '''''''' + '',@overwrite_S3_backup_file=1;''
EXEC (@SQL)

--IF (select DATEPART ( DW , getdate() ))  = 1
--BEGIN
--	--Full backup if it is Sunday
--	SET @SQL = ''exec msdb.dbo.rds_backup_database @source_db_name=''''COMMON_WEB_V2'''','' + ''@s3_arn_to_backup_to='' + '''''''' + ''arn:aws:s3:::capi-db-prod-backup.sonicdrivein.com/COMMON_WEB_V2_'' + @backupdate + ''.bak'' + '''''''' + '',@overwrite_S3_backup_file=1;''
--	EXEC (@SQL)
--END
--
--ELSE
--BEGIN
--	--Differential backup if it is Not Sunday.
--	SET @SQL = ''exec msdb.dbo.rds_backup_database @source_db_name=''''COMMON_WEB_V2'''','' + ''@s3_arn_to_backup_to='' + '''''''' + ''arn:aws:s3:::capi-db-prod-backup.sonicdrivein.com/COMMON_WEB_V2_'' + @backupdate + ''_DIFF.bak'' + '''''''' + '',@overwrite_S3_backup_file=1,@type=''''differential'''';''
--	EXEC (@SQL)
--END

SET @SQL = ''exec msdb.dbo.rds_backup_database @source_db_name=''''DPS_SONIC'''','' + ''@s3_arn_to_backup_to='' + '''''''' + ''arn:aws:s3:::capi-db-prod-backup.sonicdrivein.com/DPS_SONIC_'' + @backupdate + ''.bak'' + '''''''' + '',@overwrite_S3_backup_file=1;''
EXEC (@SQL)

SET @SQL = ''exec msdb.dbo.rds_backup_database @source_db_name=''''SonicAPI'''','' + ''@s3_arn_to_backup_to='' + '''''''' + ''arn:aws:s3:::capi-db-prod-backup.sonicdrivein.com/SonicAPI_'' + @backupdate + ''.bak'' + '''''''' + '',@overwrite_S3_backup_file=1;''
EXEC (@SQL)

SET @SQL = ''exec msdb.dbo.rds_backup_database @source_db_name=''''COMMON_WEB_V2'''','' + ''@s3_arn_to_backup_to='' + '''''''' + ''arn:aws:s3:::capi-db-prod-backup.sonicdrivein.com/COMMON_WEB_V2_'' + @backupdate + ''.bak'' + '''''''' + '',@overwrite_S3_backup_file=1;''
EXEC (@SQL)', 
		@database_name=N'master', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'1 AM', 
		@enabled=1, 
		@freq_type=4, 
		@freq_interval=1, 
		@freq_subday_type=1, 
		@freq_subday_interval=0, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=0, 
		@active_start_date=20170914, 
		@active_end_date=99991231, 
		@active_start_time=10000, 
		@active_end_time=235959, 
		@schedule_uid=N'77009f8b-6ca0-4739-b3c5-0a0d7943eaa2'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:
GO

