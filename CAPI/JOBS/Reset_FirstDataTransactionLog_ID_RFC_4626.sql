USE [msdb]
GO

/****** Object:  Job [Reset_FirstDataTransactionLog_ID_RFC_4626]    Script Date: 11/5/2018 11:12:44 AM ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [[Uncategorized (Local)]]    Script Date: 11/5/2018 11:12:44 AM ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'[Uncategorized (Local)]' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'[Uncategorized (Local)]'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'Reset_FirstDataTransactionLog_ID_RFC_4626', 
		@enabled=1, 
		@notify_level_eventlog=0, 
		@notify_level_email=0, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'No description available.', 
		@category_name=N'[Uncategorized (Local)]', 
		@owner_login_name=N'sonicadmin', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [execute SQL]    Script Date: 11/5/2018 11:12:44 AM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'execute SQL', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'INSERT SonicAPI.dbo.Max_Transaction_Reference_ID_Before_Archive
SELECT MAX(TransactionReferenceID),getdate() FROM [COMMON_WEB_V2].[dbo].[FirstDataTransactionLog] with (NOLOCK);
GO
BEGIN TRANSACTION
INSERT [dbo].[FirstDataTransactionLog_Backup]
SELECT * FROM FirstDataTransactionLog;
DELETE FROM FirstDataTransactionLog;
DBCC CHECKIDENT (''FirstDataTransactionLog'', RESEED, 1) WITH NO_INFOMSGS ;
--Reset the Sequence (part of RFC 4710, added 10/1/18)
ALTER SEQUENCE dbo.FirstData_TransactionReferenceID
	RESTART WITH 1;

COMMIT', 
		@database_name=N'COMMON_WEB_V2', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'Weekly_Monday_805_AM_GMT', 
		@enabled=1, 
		@freq_type=8, 
		@freq_interval=2, 
		@freq_subday_type=1, 
		@freq_subday_interval=0, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=1, 
		@active_start_date=20180907, 
		@active_end_date=99991231, 
		@active_start_time=80500, 
		@active_end_time=235959, 
		@schedule_uid=N'e1419a13-935d-4811-acf3-391cb4207995'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:
GO

