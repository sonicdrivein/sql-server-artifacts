USE [msdb]
GO

/****** Object:  Job [jbs_Report_ Routing_Email_Replacement]    Script Date: 11/8/2018 2:56:54 PM ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [[Uncategorized (Local)]]    Script Date: 11/8/2018 2:56:54 PM ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'[Uncategorized (Local)]' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'[Uncategorized (Local)]'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'jbs_Report_ Routing_Email_Replacement', 
		@enabled=1, 
		@notify_level_eventlog=0, 
		@notify_level_email=0, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'This replaces the email and archive functionality of Report Routing until the last financial reports can be moved into Cognos.', 
		@category_name=N'[Uncategorized (Local)]', 
		@owner_login_name=N'sa', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Email_Reports]    Script Date: 11/8/2018 2:56:55 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Email_Reports', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=4, 
		@on_fail_step_id=3, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'DECLARE @Command varchar(MAX)

SELECT DT.message_id, HD.[routing_destination], GP.[email_from], GP.[email_subject], GP.[email_body], DT.[file_path] INTO #REPORT
FROM [db_ReportRouting].[dbo].[tReport_routing_work_detail] DT
	INNER JOIN [db_ReportRouting].[dbo].[tReport_routing_work_header] HD
		ON DT.message_id = HD.message_id
			AND HD.routing_destination <> ''@sonicdrivein.com''
	INNER JOIN [db_ReportRouting].[dbo].[treport_master_group] GP
		ON HD.report_group_id = GP.report_group_id
			AND GP.report_group_id NOT IN (20)

DECLARE Email_Cursor CURSOR FOR 
SELECT 
''msdb.dbo.sp_send_dbmail @profile_name =  ''''SONIC''''   
	 ,  @recipients = '''''' + [routing_destination] + ''''''  
     ,  @from_address =  '''''' + [email_from] + ''''''   
     ,  @subject =  '''''' + [email_subject] + ''''''   
     ,  @body =  '''''' + [email_body] + ''''''  
	 ,  @reply_to = '''''' + [email_from] + ''''''
     ,  @file_attachments = ''''\\drivein\specfolders\SonicAppsData\Report_Routing\download\'' + [file_path] + ''''''    ''
  FROM #REPORT

OPEN Email_Cursor
FETCH NEXT FROM Email_Cursor INTO @Command

WHILE @@FETCH_STATUS = 0 
BEGIN
	EXEC (@Command)
	FETCH NEXT FROM Email_Cursor INTO @Command 
END 

--SELECT DT.*
DELETE DT
  FROM [db_ReportRouting].[dbo].[tReport_routing_work_detail] DT
	INNER JOIN #REPORT R
		ON DT.message_id = R.message_id

--SELECT HD.*
DELETE HD
  FROM [db_ReportRouting].[dbo].[tReport_routing_work_header] HD
	INNER JOIN #REPORT R
		ON HD.message_id = R.message_id
  
CLOSE Email_Cursor
DEALLOCATE Email_Cursor
DROP TABLE #REPORT', 
		@database_name=N'db_ReportRouting', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Archive_Reports]    Script Date: 11/8/2018 2:56:55 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Archive_Reports', 
		@step_id=2, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=4, 
		@on_fail_step_id=3, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'CmdExec', 
		@command=N'powershell.exe -command Move-Item -Path "\\drivein\specfolders\SonicAppsData\Report_Routing\download\\*.pdf" -Destination "\\drivein\specfolders\SonicAppsData\Report_Routing\Archive" -Force', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Log_Failure_Ticket]    Script Date: 11/8/2018 2:56:55 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Log_Failure_Ticket', 
		@step_id=3, 
		@cmdexec_success_code=0, 
		@on_success_action=2, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'EXEC dbo.spJobNotification2005dbmail @jobname = ''jbs_Report_ Routing_Email_Replacement''', 
		@database_name=N'SAonly', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'Hourly', 
		@enabled=1, 
		@freq_type=4, 
		@freq_interval=1, 
		@freq_subday_type=8, 
		@freq_subday_interval=1, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=0, 
		@active_start_date=20181030, 
		@active_end_date=99991231, 
		@active_start_time=130500, 
		@active_end_time=230500, 
		@schedule_uid=N'cb161b6c-b389-4102-8b3f-b74f66a7ca9f'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'Morning', 
		@enabled=1, 
		@freq_type=4, 
		@freq_interval=1, 
		@freq_subday_type=1, 
		@freq_subday_interval=0, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=0, 
		@active_start_date=20181030, 
		@active_end_date=99991231, 
		@active_start_time=90000, 
		@active_end_time=235959, 
		@schedule_uid=N'9095f528-3c8b-4a57-a6d8-1d85fa6e9dd3'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:
GO


