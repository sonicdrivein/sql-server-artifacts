USE [msdb]
GO

/****** Object:  Job [jbs_R2W_Fix]    Script Date: 11/7/2018 10:58:55 AM ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [[Uncategorized (Local)]]    Script Date: 11/7/2018 10:58:55 AM ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'[Uncategorized (Local)]' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'[Uncategorized (Local)]'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'jbs_R2W_Fix', 
		@enabled=1, 
		@notify_level_eventlog=0, 
		@notify_level_email=0, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'No description available.', 
		@category_name=N'[Uncategorized (Local)]', 
		@owner_login_name=N'sa', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Catalog_Builder_Replacement]    Script Date: 11/7/2018 10:58:55 AM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Catalog_Builder_Replacement', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=4, 
		@on_fail_step_id=2, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'EXECUTE [dbo].[r2w_updateDocs]

UPDATE D
SET D.folderId = D2.folderId
FROM [Enterprise].[dbo].[documents] D
	INNER JOIN [Enterprise].[dbo].[documents] D2
		ON D.folder = D2.folder
			AND D2.folderId <> 0
			AND D2.fileDate >= DATEADD(DD,-62,GETDATE())

WHERE D.folderId = 0

UPDATE D
SET D.[description] = D2.[description]
FROM [Enterprise].[dbo].[documents] D
	INNER JOIN [Enterprise].[dbo].[documents] D2
		ON D.[reportName] = D2.[reportName]
			AND D2.[description] <> ''''
			AND D2.fileDate >= DATEADD(DD,-62,GETDATE())

WHERE D.[description] = ''''

EXECUTE [dbo].[r2w_updateDocuments1] ''plstore-sum''
EXECUTE [dbo].[r2w_updateDocuments1] ''plorg-dtl''
EXECUTE [dbo].[r2w_updateDocuments1] ''plstore-dtl''
EXECUTE [dbo].[r2w_updateDocuments1] ''balshtsupv-dtl''
EXECUTE [dbo].[r2w_updateDocuments1] ''plsupv-dtl''
EXECUTE [dbo].[r2w_updateDocuments1] ''catalog_scan''
EXECUTE [dbo].[r2w_updateDocuments1] ''balshtrvp-sum''
EXECUTE [dbo].[r2w_updateDocuments1] ''publishlogfile''
EXECUTE [dbo].[r2w_updateDocuments1] ''pldir-sum''
EXECUTE [dbo].[r2w_updateDocuments1] ''GLStoreDtl''
EXECUTE [dbo].[r2w_updateDocuments1] ''balshtorg-sum''
EXECUTE [dbo].[r2w_updateDocuments1] ''plsupv-sum''
EXECUTE [dbo].[r2w_updateDocuments1] ''pldir-dtl''
EXECUTE [dbo].[r2w_updateDocuments1] ''pltcrinv''
EXECUTE [dbo].[r2w_updateDocuments1] ''balshtrvp-dtl''
EXECUTE [dbo].[r2w_updateDocuments1] ''plorg-sum''
EXECUTE [dbo].[r2w_updateDocuments1] ''balshtdir-sum''
EXECUTE [dbo].[r2w_updateDocuments1] ''balshtorg-dtl''
EXECUTE [dbo].[r2w_updateDocuments1] ''balshtsupv-sum''
EXECUTE [dbo].[r2w_updateDocuments1] ''plrvp-sum''
EXECUTE [dbo].[r2w_updateDocuments1] ''balshtstore-dtl''
EXECUTE [dbo].[r2w_updateDocuments1] ''balshtstore-sum''
EXECUTE [dbo].[r2w_updateDocuments1] ''balshtdir-dtl''
EXECUTE [dbo].[r2w_calculateDepth] 

UPDATE [Enterprise].[dbo].[documents]
SET [title] = [reportName]
WHERE [title] = ''@reportname@''', 
		@database_name=N'Enterprise', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [SNOW Email]    Script Date: 11/7/2018 10:58:55 AM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'SNOW Email', 
		@step_id=2, 
		@cmdexec_success_code=0, 
		@on_success_action=2, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'EXEC dbo.spJobNotification2005dbmail @jobname = ''jbs_R2W_Fix''', 
		@database_name=N'SAonly', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'Every 30 minutes', 
		@enabled=1, 
		@freq_type=4, 
		@freq_interval=1, 
		@freq_subday_type=4, 
		@freq_subday_interval=30, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=0, 
		@active_start_date=20181107, 
		@active_end_date=99991231, 
		@active_start_time=91000, 
		@active_end_time=234100, 
		@schedule_uid=N'8cc6b146-3341-4c83-a4ad-c2d5cbe95dfc'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:
GO


