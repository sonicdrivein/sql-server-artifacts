USE [msdb]
GO

/****** Object:  Job [jbs_Cleanup_SSIS_package_logging_table]    Script Date: 10/18/2018 2:03:00 PM ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [[Uncategorized (Local)]]    Script Date: 10/18/2018 2:03:00 PM ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'[Uncategorized (Local)]' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'[Uncategorized (Local)]'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'jbs_Cleanup_SSIS_package_logging_table', 
		@enabled=1, 
		@notify_level_eventlog=0, 
		@notify_level_email=0, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'No description available.', 
		@category_name=N'[Uncategorized (Local)]', 
		@owner_login_name=N'sa', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Truncate package log]    Script Date: 10/18/2018 2:03:00 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Truncate package log', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=3, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'truncate table msdb.dbo.sysdtslog90', 
		@database_name=N'msdb', 
		@output_file_name=N'c:\Program Files\Microsoft SQL Server\MSSQL.1\MSSQL\LOG\jbs_Cleanup_SSIS_package_logging_table.txt', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [HEAT Email]    Script Date: 10/18/2018 2:03:00 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'HEAT Email', 
		@step_id=2, 
		@cmdexec_success_code=0, 
		@on_success_action=2, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'EXEC dbo.spJobNotification2005dbmail @jobname = ''jbs_Cleanup_SSIS_package_logging_table''', 
		@database_name=N'SAOnly', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'Hourly', 
		@enabled=1, 
		@freq_type=4, 
		@freq_interval=1, 
		@freq_subday_type=8, 
		@freq_subday_interval=1, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=0, 
		@active_start_date=20090508, 
		@active_end_date=99991231, 
		@active_start_time=0, 
		@active_end_time=235959
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:
GO

/****** Object:  Job [jbs_Monthly_Database_Mail_Cleanup]    Script Date: 10/18/2018 2:03:00 PM ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [[Uncategorized (Local)]]    Script Date: 10/18/2018 2:03:00 PM ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'[Uncategorized (Local)]' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'[Uncategorized (Local)]'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'jbs_Monthly_Database_Mail_Cleanup', 
		@enabled=1, 
		@notify_level_eventlog=2, 
		@notify_level_email=0, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'No description available.', 
		@category_name=N'[Uncategorized (Local)]', 
		@owner_login_name=N'sa', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [stp_Remove_rows_from_Database_Mail]    Script Date: 10/18/2018 2:03:00 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'stp_Remove_rows_from_Database_Mail', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=4, 
		@on_fail_step_id=3, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'DECLARE @CopyDate nvarchar(20) ;
SET @CopyDate = (SELECT CAST(CONVERT(char(8), CURRENT_TIMESTAMP- DATEPART(dd,GETDATE()-1), 112) AS datetime)) ;
EXECUTE msdb.dbo.sysmail_delete_mailitems_sp @sent_before = @CopyDate ;', 
		@database_name=N'msdb', 
		@output_file_name=N'c:\Program Files\Microsoft SQL Server\MSSQL.1\MSSQL\LOG\jbs_Monthly_Database_Mail_Cleanup.txt', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [stp_Remove_rows_from_Database_Mail_Log]    Script Date: 10/18/2018 2:03:00 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'stp_Remove_rows_from_Database_Mail_Log', 
		@step_id=2, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=3, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'DECLARE @CopyDate nvarchar(20) ;
SET @CopyDate = (SELECT CAST(CONVERT(char(8), CURRENT_TIMESTAMP- DATEPART(dd,GETDATE()-1), 112) AS datetime)) ;
EXECUTE msdb.dbo.sysmail_delete_log_sp @logged_before = @CopyDate ;', 
		@database_name=N'msdb', 
		@output_file_name=N'c:\Program Files\Microsoft SQL Server\MSSQL.1\MSSQL\LOG\jbs_Monthly_Database_Mail_Cleanup.txt', 
		@flags=2
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Heat Email]    Script Date: 10/18/2018 2:03:00 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Heat Email', 
		@step_id=3, 
		@cmdexec_success_code=0, 
		@on_success_action=2, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'EXEC dbo.spJobNotification2005dbmail @jobname = ''jbs_Monthly_Database_Mail_Cleanup''', 
		@database_name=N'SAOnly', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'Monthly', 
		@enabled=1, 
		@freq_type=16, 
		@freq_interval=1, 
		@freq_subday_type=1, 
		@freq_subday_interval=0, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=1, 
		@active_start_date=20080301, 
		@active_end_date=99991231, 
		@active_start_time=40000, 
		@active_end_time=235959
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:
GO

/****** Object:  Job [jbs_po_Create_and_Send_Combined_Flash_Reports]    Script Date: 10/18/2018 2:03:00 PM ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [[Uncategorized (Local)]]    Script Date: 10/18/2018 2:03:00 PM ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'[Uncategorized (Local)]' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'[Uncategorized (Local)]'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'jbs_po_Create_and_Send_Combined_Flash_Reports', 
		@enabled=1, 
		@notify_level_eventlog=0, 
		@notify_level_email=0, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'No description available.', 
		@category_name=N'[Uncategorized (Local)]', 
		@owner_login_name=N'sa', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Create NONSRI flash reports]    Script Date: 10/18/2018 2:03:00 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Create NONSRI flash reports', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=4, 
		@on_fail_step_id=7, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'declare @strFlashRpts varchar(300)
declare @theDate varchar(10)

set @theDate = convert(varchar, getdate(), 101)

set @strFlashRpts = ''D:\FlashReportsExcel\New\FlashReportsExcel.exe '' + @theDate + '' '' + convert(varchar, 1)

exec xp_cmdshell @strFlashRpts
', 
		@database_name=N'master', 
		@output_file_name=N'c:\Program Files\Microsoft SQL Server\MSSQL.1\MSSQL\LOG\jbs_po_Create_and_Send_Combined_Flash_Reports.txt', 
		@flags=2
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Send Alvin McQuilliams Flash Report]    Script Date: 10/18/2018 2:03:00 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Send Alvin McQuilliams Flash Report', 
		@step_id=2, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=4, 
		@on_fail_step_id=7, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'declare @thedate varchar(8)
declare @strFlashReportsPath varchar(255)
declare @strAttachments varchar(1000)
declare @result int

set @thedate = replace(convert(varchar, getdate(), 1), ''/'', '''')

set @strFlashReportsPath = ''\\DBSQLPROD01\f_drive\Flash Reports\'' + @thedate

select @strAttachments = @strFlashReportsPath + ''\McQuilliams_Alvin_106548_'' + @thedate + '' (Vice President).xls''

--Adding 3 additional email recipients bydir Alvin McQuilliams on 12/19/2011. . . . Latch APIN-1274

EXEC @result = xp_cmdshell ''dir '''' + @strAttachments + '''''', no_output
IF (@result > 0)
	BEGIN
		EXEC  msdb..sp_send_dbmail  
		@profile_name = ''polling'',
		@recipients = ''Alvin-M@sbcglobal.net; cameron.mcquilliams@sonicpartnernet.com; shawn.mcquilliams@sonicpartnernet.com; mhamilton@sonicpartnernet.com'', 
		@copy_recipients = ''ApplicationIntegrity@sonicdrivein.com'',
		@subject = ''Daily Flash Reports'',
		@body = ''McQuilliams workbook, per request.'',
		@file_attachments = @strAttachments
	END
', 
		@database_name=N'msdb', 
		@output_file_name=N'c:\Program Files\Microsoft SQL Server\MSSQL.1\MSSQL\LOG\jbs_po_Create_and_Send_Combined_Flash_Reports.txt', 
		@flags=2
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Send Great Lakes Flash]    Script Date: 10/18/2018 2:03:00 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Send Great Lakes Flash', 
		@step_id=3, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=4, 
		@on_fail_step_id=7, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'declare @thedate varchar(8)
declare @strFlashReportsPath varchar(255)
declare @strAttachments varchar(1000)
declare @result int

set @thedate = replace(convert(varchar, getdate(), 1), ''/'', '''')

set @strFlashReportsPath = ''\\DBSQLPROD01\f_drive\Flash Reports\'' + @thedate

select @strAttachments = @strFlashReportsPath + ''\Perry_Mike_124115_'' + @thedate + '' (Vice President).xls''

EXEC @result = xp_cmdshell ''dir '''' + @strAttachments + '''''', no_output
IF (@result > 0)
	BEGIN
		EXEC  msdb..sp_send_dbmail  
		@profile_name = ''polling'',
		@recipients = ''mperry1778@gmail.com'', 
		@copy_recipients = ''ApplicationIntegrity@sonicdrivein.com'',
		@subject = ''Daily Flash Reports'',
		@body = ''Daily Flash Reports'',
		@file_attachments = @strAttachments
	END
/*
-- Removed Billhorn from distribution per MPerry 3-16-2015 Performed by RLucena	
select @strAttachments = @strFlashReportsPath + ''\Billhorn_Julie_333372_'' + @thedate + '' (Supervisor).xls''

EXEC @result = xp_cmdshell ''dir '''' + @strAttachments + '''''', no_output
IF (@result > 0)
	BEGIN
		EXEC  msdb..sp_send_dbmail  
		@profile_name = ''polling'',
		@recipients = ''jbillhornsonic@gmail.com'', 
		@copy_recipients = ''ApplicationIntegrity@sonicdrivein.com'',
		@subject = ''Daily Flash Reports'',
		@body = ''Daily Flash Reports'',
		@file_attachments = @strAttachments
	END	

*/

/*	
---------------------------------------------------------------------------------------------------------------------
--APIN-1273(LATCH) Adding Doug Cole to the flash report distro on 12/16/2011.
---------------------------------------------------------------------------------------------------------------------
	select @strAttachments = @strFlashReportsPath + ''\Cole_Douglas_107367_'' + @thedate + '' (Vice President).xls''

EXEC @result = xp_cmdshell ''dir '''' + @strAttachments + '''''', no_output
IF (@result > 0)
	BEGIN
		EXEC  msdb..sp_send_dbmail  
		@profile_name = ''polling'',
		@recipients = ''coledebra22@yahoo.com; store3129@sonicpartnernet.com'', 
		@copy_recipients = ''ApplicationIntegrity@sonicdrivein.com'',
		@subject = ''Daily Flash Reports'',
		@body = ''Daily Flash Reports'',
		@file_attachments = @strAttachments
	END
*/
	
---------------------------------------------------------------------------------------------------------------------
--APIN-1295(LATCH) Adding Joel Garzas group to this flash job step on 2/3/2012.
---------------------------------------------------------------------------------------------------------------------
	
--select @strAttachments = @strFlashReportsPath + ''\LLC_TXSDI31_143141_'' + @thedate + '' (Vice President).xls''

--EXEC @result = xp_cmdshell ''dir '''' + @strAttachments + '''''', no_output
--IF (@result > 0)
--	BEGIN
--		EXEC  msdb..sp_send_dbmail  
--		@profile_name = ''polling'',
--		@recipients = ''garza-mgmt@comcast.net; capello.mike@yahoo.com; R.Sunesara@gmail.com;jharper-garzagroup@comcast.net;Carrasco_1985@outlook.com;angarza12@gmail.com'', 
--		@copy_recipients = ''ApplicationIntegrity@sonicdrivein.com'',
--		@subject = ''Daily Flash Reports'',
--		@body = ''Daily Flash Reports'',
--		@file_attachments = @strAttachments
--	END', 
		@database_name=N'msdb', 
		@output_file_name=N'c:\Program Files\Microsoft SQL Server\MSSQL.1\MSSQL\LOG\jbs_po_Create_and_Send_Combined_Flash_Reports.txt', 
		@flags=2
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Send Exceptional Flash]    Script Date: 10/18/2018 2:03:00 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Send Exceptional Flash', 
		@step_id=4, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=4, 
		@on_fail_step_id=7, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'declare @thedate varchar(8)
declare @strFlashReportsPath varchar(255)
declare @strAttachments varchar(1000)
declare @result int

set @thedate = replace(convert(varchar, getdate(), 1), ''/'', '''')

set @strFlashReportsPath = ''\\DBSQLPROD01\f_drive\Flash Reports\'' + @thedate

----------------------------------------------------------------------------------------------------
--Change requested by Kelly Sellers (Controller: Exceptional Restaurants).   Replace Anthony Fontana
--with Jim Ellison on 5/16/2011
----------------------------------------------------------------------------------------------------

/*
select @strAttachments = @strFlashReportsPath + ''\Ellison_Jim_333351_'' + @thedate + '' (Supervisor).xls''

EXEC @result = xp_cmdshell ''dir '''' + @strAttachments + '''''', no_output
IF (@result > 0)
	BEGIN
		EXEC  msdb..sp_send_dbmail  
		@profile_name = ''polling'',
		@recipients = ''Jim@teamerc.com'', 
		@copy_recipients = ''ApplicationIntegrity@sonicdrivein.com'',
		@subject = ''Daily Flash Reports'',
		@body = ''Supervisor Flash Report.'',
		@file_attachments = @strAttachments
	END
*/
-------------------------------------------------------------------------------------------------------------
--"Kelly Sellers" (fname/lname) changed to "SONIC FLASH" on 5/19/2011 in order to accommodate new recipient of
--director level flash for exceptional, Ayman Amin.  Per request of Kelly Sellers.  
--------------------------------------------------------------------------------------------------------------
/*

select @strAttachments = @strFlashReportsPath + ''\FLASH_SONIC_333343_'' + @thedate + '' (Director).xls''
--select @strAttachments

EXEC @result = xp_cmdshell ''dir '''' + @strAttachments + '''''', no_output
IF (@result > 0)
	BEGIN
		EXEC  msdb..sp_send_dbmail  
		@profile_name = ''polling'',
		@recipients = ''sonicflash@teamerc.com'', 
		@copy_recipients = ''ApplicationIntegrity@sonicdrivein.com'',
		@subject = ''Daily Flash Reports'',
		@body = ''Supervisor Flash Report.'',
		@file_attachments = @strAttachments
	END
*/

select @strAttachments = @strFlashReportsPath + ''\Luther_Debra_333344_'' + @thedate + '' (regional vice president).xls''
select @strAttachments

EXEC @result = xp_cmdshell ''dir '''' + @strAttachments + '''''', no_output
IF (@result > 0)
	BEGIN
		EXEC  msdb..sp_send_dbmail  
		@profile_name = ''polling'',
		@recipients = ''debra@exceptionalrestaurants.com'', 
		@copy_recipients = ''ApplicationIntegrity@sonicdrivein.com'',
		@subject = ''Daily Flash Reports'',
		@body = ''Supervisor Flash Report.'',
		@file_attachments = @strAttachments
	END

/*
select @strAttachments = @strFlashReportsPath + ''\Embrey_Vickey_333352_'' + @thedate + '' (Supervisor).xls''
select @strAttachments

EXEC @result = xp_cmdshell ''dir '''' + @strAttachments + '''''', no_output
IF (@result > 0)
	BEGIN
		EXEC  msdb..sp_send_dbmail  
		@profile_name = ''polling'',
		@recipients = ''vembrey@teamerc.com'', 
		@copy_recipients = ''ApplicationIntegrity@sonicdrivein.com'',
		@subject = ''Daily Flash Reports'',
		@body = ''Supervisor Flash Report.'',
		@file_attachments = @strAttachments
	END
*/


select @strAttachments = @strFlashReportsPath + ''\Thomas_Jerome_333345_'' + @thedate + '' (vice president).xls''
select @strAttachments

EXEC @result = xp_cmdshell ''dir '''' + @strAttachments + '''''', no_output
IF (@result > 0)
	BEGIN
		EXEC  msdb..sp_send_dbmail  
		@profile_name = ''polling'',
		@recipients = ''jerome@exceptionalrestaurants.com'', 
		@copy_recipients = ''ApplicationIntegrity@sonicdrivein.com'',
		@subject = ''Daily Flash Reports'',
		@body = ''Supervisor Flash Report.'',
		@file_attachments = @strAttachments
	END

', 
		@database_name=N'msdb', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Send DL Rogers Flash]    Script Date: 10/18/2018 2:03:00 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Send DL Rogers Flash', 
		@step_id=5, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=4, 
		@on_fail_step_id=7, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'declare @thedate varchar(8)
declare @strFlashReportsPath varchar(255)
declare @strAttachments varchar(1000)
declare @result int

set @thedate = replace(convert(varchar, getdate(), 1), ''/'', '''')

set @strFlashReportsPath = ''\\DBSQLPROD01\f_drive\Flash Reports\'' + @thedate

/*
select @strAttachments = @strFlashReportsPath + ''\Vandelicht_Kevin_333452_'' + @thedate + '' (supervisor).xls''
/* select @strAttachments */

EXEC @result = xp_cmdshell ''dir '''' + @strAttachments + '''''', no_output
IF (@result > 0)
	BEGIN
		EXEC  msdb..sp_send_dbmail  
		@profile_name = ''polling'',
		@recipients = ''Kevin.vandelicht@sonicpartnernet.com'', 
		@copy_recipients = ''ApplicationIntegrity@sonicdrivein.com'',
		@subject = ''Daily Flash Reports'',
		@body = ''Supervisor Flash Report.'',
		@file_attachments = @strAttachments
	END
*/

select @strAttachments = @strFlashReportsPath + ''\Ayers_Jessie_333360_'' + @thedate + '' (supervisor).xls''
/* select @strAttachments */

EXEC @result = xp_cmdshell ''dir '''' + @strAttachments + '''''', no_output
IF (@result > 0)
	BEGIN
		EXEC  msdb..sp_send_dbmail  
		@profile_name = ''polling'',
		@recipients = ''jessie.ayers@sonicpartnernet.com'', 
		@copy_recipients = ''ApplicationIntegrity@sonicdrivein.com'',
		@subject = ''Daily Flash Reports'',
		@body = ''Supervisor Flash Report.'',
		@file_attachments = @strAttachments
	END


select @strAttachments = @strFlashReportsPath + ''\Cox_Diana_333518_'' + @thedate + '' (supervisor).xls''
select @strAttachments

EXEC @result = xp_cmdshell ''dir '''' + @strAttachments + '''''', no_output
IF (@result > 0)
	BEGIN
		EXEC  msdb..sp_send_dbmail  
		@profile_name = ''polling'',
		@recipients = ''Diana.Cox@sonicpartnernet.com'', 
		@copy_recipients = ''ApplicationIntegrity@sonicdrivein.com'',
		@subject = ''Daily Flash Reports'',
		@body = ''Supervisor Flash Report.'',
		@file_attachments = @strAttachments
	END

/*
select @strAttachments = @strFlashReportsPath + ''\Luther_Debbie_103787_'' + @thedate + '' (director).xls''
select @strAttachments

EXEC @result = xp_cmdshell ''dir '''' + @strAttachments + '''''', no_output
IF (@result > 0)
	BEGIN
		EXEC  msdb..sp_send_dbmail  
		@profile_name = ''polling'',
		@recipients = ''debbie.luther@sonicpartnernet.com'', 
		@copy_recipients = ''ApplicationIntegrity@sonicdrivein.com'',
		@subject = ''Daily Flash Reports'',
		@body = ''Supervisor Flash Report.'',
		@file_attachments = @strAttachments
	END
*/


/*
select @strAttachments = @strFlashReportsPath + ''\CORP_D.L.ROGERS_107983_'' + @thedate + '' (Vice President).xls''
select @strAttachments

EXEC @result = xp_cmdshell ''dir '''' + @strAttachments + '''''', no_output
IF (@result > 0)
	BEGIN
		EXEC  msdb..sp_send_dbmail  
		@profile_name = ''polling'',
		@recipients = ''jjunkin@dlrogers.com;krhoades@dlrogers.com'', 
		@copy_recipients = ''ApplicationIntegrity@sonicdrivein.com'',
		@subject = ''Daily Flash Reports'',
		@body = ''Supervisor Flash Report.'',
		@file_attachments = @strAttachments
	END
*/

/* Added so that Jim Peoples can get the RVP report, even though it contains directors that are not under him */
/*
select @strAttachments = @strFlashReportsPath + ''\CORP_D.L.ROGERS_107983_'' + @thedate + '' (Regional Vice President).xls''
select @strAttachments

EXEC @result = xp_cmdshell ''dir '''' + @strAttachments + '''''', no_output
IF (@result > 0)
	BEGIN
		EXEC  msdb..sp_send_dbmail  
		@profile_name = ''polling'',
		@recipients = ''jpeoples@dlrogers.com'', 
		@copy_recipients = ''ApplicationIntegrity@sonicdrivein.com'',
		@subject = ''Daily Flash Reports'',
		@body = ''RVP Flash Report.'',
		@file_attachments = @strAttachments
	END
*/', 
		@database_name=N'msdb', 
		@output_file_name=N'c:\Program Files\Microsoft SQL Server\MSSQL.1\MSSQL\LOG\jbs_po_Create_and_Send_Combined_Flash_Reports.txt', 
		@flags=2
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Success Page]    Script Date: 10/18/2018 2:03:00 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Success Page', 
		@step_id=6, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=4, 
		@on_fail_step_id=8, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'Use msdb	

EXEC sp_send_dbmail 
	@profile_name = ''polling'',
	@recipients =''ApplicationIntegrity@sonicdrivein.com'',
	@query = ''SELECT Getdate()'', 
	@query_result_header = ''FALSE'',
    @subject = ''Job Succeeded'',
    @body = ''jbs_po_Create_and_Send_Combined_Flash_Reports''', 
		@database_name=N'msdb', 
		@output_file_name=N'c:\Program Files\Microsoft SQL Server\MSSQL.1\MSSQL\LOG\jbs_po_Create_and_Send_Combined_Flash_Reports.txt', 
		@flags=2
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Failure Page]    Script Date: 10/18/2018 2:03:00 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Failure Page', 
		@step_id=7, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=3, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'Use msdb	
EXEC sp_send_dbmail --xp_sendmail 
	@profile_name = ''polling'',
	@recipients =''ApplicationIntegrity@sonicdrivein.com'',
	@query = ''SELECT Getdate()'', 
	@query_result_header = ''FALSE'',
    @subject = ''Job Failed'',
    @body = ''jbs_po_Create_and_Send_Combined_Flash_Reports''', 
		@database_name=N'msdb', 
		@output_file_name=N'c:\Program Files\Microsoft SQL Server\MSSQL.1\MSSQL\LOG\jbs_po_Create_and_Send_Combined_Flash_Reports.txt', 
		@flags=2
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Heat Email]    Script Date: 10/18/2018 2:03:00 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Heat Email', 
		@step_id=8, 
		@cmdexec_success_code=0, 
		@on_success_action=2, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'EXEC dbo.spJobNotification2005dbmail @jobname = ''jbs_po_Create_and_Send_Combined_Flash_Reports''', 
		@database_name=N'SAOnly', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:
GO

/****** Object:  Job [jbs_zipstripper]    Script Date: 10/18/2018 2:03:00 PM ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [[Uncategorized (Local)]]    Script Date: 10/18/2018 2:03:00 PM ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'[Uncategorized (Local)]' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'[Uncategorized (Local)]'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'jbs_zipstripper', 
		@enabled=1, 
		@notify_level_eventlog=0, 
		@notify_level_email=0, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'No description available.', 
		@category_name=N'[Uncategorized (Local)]', 
		@owner_login_name=N'sa', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [run strip.vbs]    Script Date: 10/18/2018 2:03:00 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'run strip.vbs', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=3, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'CmdExec', 
		@command=N'wscript D:\ETL_Apps\OML\zipstripper\strip.vbs', 
		@output_file_name=N'c:\Program Files\Microsoft SQL Server\MSSQL.1\MSSQL\LOG\jbs_zipstripper.txt', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Heat Email]    Script Date: 10/18/2018 2:03:00 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Heat Email', 
		@step_id=2, 
		@cmdexec_success_code=0, 
		@on_success_action=2, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'EXEC dbo.spJobNotification2005dbmail @jobname = ''jbs_zipstripper''', 
		@database_name=N'SAOnly', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'daily', 
		@enabled=1, 
		@freq_type=4, 
		@freq_interval=1, 
		@freq_subday_type=1, 
		@freq_subday_interval=0, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=0, 
		@active_start_date=20061110, 
		@active_end_date=99991231, 
		@active_start_time=43500, 
		@active_end_time=235959
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:
GO

/****** Object:  Job [OMC Load 1]    Script Date: 10/18/2018 2:03:00 PM ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [[Uncategorized (Local)]]    Script Date: 10/18/2018 2:03:00 PM ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'[Uncategorized (Local)]' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'[Uncategorized (Local)]'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'OMC Load 1', 
		@enabled=1, 
		@notify_level_eventlog=0, 
		@notify_level_email=0, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'Ordermatic load process', 
		@category_name=N'[Uncategorized (Local)]', 
		@owner_login_name=N'sa', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Load OMC Data]    Script Date: 10/18/2018 2:03:00 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Load OMC Data', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=3, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'SSIS', 
		@command=N'/FILE "\\drivein\SSIS_Repository\OMLoads2.dtsx" /DECRYPT sonic /CHECKPOINTING OFF /REPORTING E', 
		@database_name=N'master', 
		@output_file_name=N'c:\Program Files\Microsoft SQL Server\MSSQL.1\MSSQL\LOG\OMC Load 1.txt', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [HEAT Email]    Script Date: 10/18/2018 2:03:00 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'HEAT Email', 
		@step_id=2, 
		@cmdexec_success_code=0, 
		@on_success_action=2, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'EXEC dbo.spJobNotification2005dbmail @jobname = ''OMC Load''', 
		@database_name=N'SAOnly', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'Continuous', 
		@enabled=1, 
		@freq_type=64, 
		@freq_interval=0, 
		@freq_subday_type=0, 
		@freq_subday_interval=0, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=0, 
		@active_start_date=20090416, 
		@active_end_date=99991231, 
		@active_start_time=0, 
		@active_end_time=235959
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:
GO

/****** Object:  Job [OMC Load 2]    Script Date: 10/18/2018 2:03:00 PM ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [[Uncategorized (Local)]]    Script Date: 10/18/2018 2:03:00 PM ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'[Uncategorized (Local)]' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'[Uncategorized (Local)]'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'OMC Load 2', 
		@enabled=1, 
		@notify_level_eventlog=0, 
		@notify_level_email=0, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'Ordermatic load process', 
		@category_name=N'[Uncategorized (Local)]', 
		@owner_login_name=N'sa', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Load OMC Data]    Script Date: 10/18/2018 2:03:00 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Load OMC Data', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=3, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'SSIS', 
		@command=N'/FILE "\\drivein\SSIS_Repository\OMLoads2.dtsx" /DECRYPT sonic /CHECKPOINTING OFF /REPORTING E', 
		@database_name=N'master', 
		@output_file_name=N'c:\Program Files\Microsoft SQL Server\MSSQL.1\MSSQL\LOG\OMC Load 1.txt', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [HEAT Email]    Script Date: 10/18/2018 2:03:00 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'HEAT Email', 
		@step_id=2, 
		@cmdexec_success_code=0, 
		@on_success_action=2, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'EXEC dbo.spJobNotification2005dbmail @jobname = ''OMC Load''', 
		@database_name=N'SAOnly', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'Continuous', 
		@enabled=1, 
		@freq_type=64, 
		@freq_interval=0, 
		@freq_subday_type=0, 
		@freq_subday_interval=0, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=0, 
		@active_start_date=20090416, 
		@active_end_date=99991231, 
		@active_start_time=0, 
		@active_end_time=235959
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:
GO

/****** Object:  Job [OMC Load 3]    Script Date: 10/18/2018 2:03:00 PM ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [[Uncategorized (Local)]]    Script Date: 10/18/2018 2:03:00 PM ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'[Uncategorized (Local)]' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'[Uncategorized (Local)]'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'OMC Load 3', 
		@enabled=1, 
		@notify_level_eventlog=0, 
		@notify_level_email=0, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'Ordermatic load process', 
		@category_name=N'[Uncategorized (Local)]', 
		@owner_login_name=N'sa', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Load OMC Data]    Script Date: 10/18/2018 2:03:01 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Load OMC Data', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=3, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'SSIS', 
		@command=N'/FILE "\\drivein\SSIS_Repository\OMLoads2.dtsx" /DECRYPT sonic /CHECKPOINTING OFF /REPORTING E', 
		@database_name=N'master', 
		@output_file_name=N'c:\Program Files\Microsoft SQL Server\MSSQL.1\MSSQL\LOG\OMC Load 1.txt', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [HEAT Email]    Script Date: 10/18/2018 2:03:01 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'HEAT Email', 
		@step_id=2, 
		@cmdexec_success_code=0, 
		@on_success_action=2, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'EXEC dbo.spJobNotification2005dbmail @jobname = ''OMC Load''', 
		@database_name=N'SAOnly', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'Continuous', 
		@enabled=1, 
		@freq_type=64, 
		@freq_interval=0, 
		@freq_subday_type=0, 
		@freq_subday_interval=0, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=0, 
		@active_start_date=20090416, 
		@active_end_date=99991231, 
		@active_start_time=0, 
		@active_end_time=235959
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:
GO

/****** Object:  Job [OMC Load 4]    Script Date: 10/18/2018 2:03:01 PM ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [[Uncategorized (Local)]]    Script Date: 10/18/2018 2:03:01 PM ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'[Uncategorized (Local)]' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'[Uncategorized (Local)]'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'OMC Load 4', 
		@enabled=1, 
		@notify_level_eventlog=0, 
		@notify_level_email=0, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'Ordermatic load process', 
		@category_name=N'[Uncategorized (Local)]', 
		@owner_login_name=N'sa', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Load OMC Data]    Script Date: 10/18/2018 2:03:01 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Load OMC Data', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=3, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'SSIS', 
		@command=N'/FILE "\\drivein\SSIS_Repository\OMLoads2.dtsx" /DECRYPT sonic /CHECKPOINTING OFF /REPORTING E', 
		@database_name=N'master', 
		@output_file_name=N'c:\Program Files\Microsoft SQL Server\MSSQL.1\MSSQL\LOG\OMC Load 1.txt', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [HEAT Email]    Script Date: 10/18/2018 2:03:01 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'HEAT Email', 
		@step_id=2, 
		@cmdexec_success_code=0, 
		@on_success_action=2, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'EXEC dbo.spJobNotification2005dbmail @jobname = ''OMC Load''', 
		@database_name=N'SAOnly', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'Continuous', 
		@enabled=1, 
		@freq_type=64, 
		@freq_interval=0, 
		@freq_subday_type=0, 
		@freq_subday_interval=0, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=0, 
		@active_start_date=20090416, 
		@active_end_date=99991231, 
		@active_start_time=0, 
		@active_end_time=235959
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:
GO

/****** Object:  Job [OMC Load 5]    Script Date: 10/18/2018 2:03:01 PM ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [[Uncategorized (Local)]]    Script Date: 10/18/2018 2:03:01 PM ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'[Uncategorized (Local)]' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'[Uncategorized (Local)]'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'OMC Load 5', 
		@enabled=1, 
		@notify_level_eventlog=0, 
		@notify_level_email=0, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'Ordermatic load process', 
		@category_name=N'[Uncategorized (Local)]', 
		@owner_login_name=N'sa', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Load OMC Data]    Script Date: 10/18/2018 2:03:01 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Load OMC Data', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=3, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'SSIS', 
		@command=N'/FILE "\\drivein\SSIS_Repository\OMLoads2.dtsx" /DECRYPT sonic /CHECKPOINTING OFF /REPORTING E', 
		@database_name=N'master', 
		@output_file_name=N'c:\Program Files\Microsoft SQL Server\MSSQL.1\MSSQL\LOG\OMC Load 1.txt', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [HEAT Email]    Script Date: 10/18/2018 2:03:01 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'HEAT Email', 
		@step_id=2, 
		@cmdexec_success_code=0, 
		@on_success_action=2, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'EXEC dbo.spJobNotification2005dbmail @jobname = ''OMC Load''', 
		@database_name=N'SAOnly', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'Continuous', 
		@enabled=1, 
		@freq_type=64, 
		@freq_interval=0, 
		@freq_subday_type=0, 
		@freq_subday_interval=0, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=0, 
		@active_start_date=20090416, 
		@active_end_date=99991231, 
		@active_start_time=0, 
		@active_end_time=235959
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:
GO

/****** Object:  Job [OMC Load 6]    Script Date: 10/18/2018 2:03:01 PM ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [[Uncategorized (Local)]]    Script Date: 10/18/2018 2:03:01 PM ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'[Uncategorized (Local)]' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'[Uncategorized (Local)]'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'OMC Load 6', 
		@enabled=1, 
		@notify_level_eventlog=0, 
		@notify_level_email=0, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'Ordermatic load process', 
		@category_name=N'[Uncategorized (Local)]', 
		@owner_login_name=N'sa', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Load OMC Data]    Script Date: 10/18/2018 2:03:01 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Load OMC Data', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=3, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'SSIS', 
		@command=N'/FILE "\\drivein\SSIS_Repository\OMLoads2.dtsx" /DECRYPT sonic /CHECKPOINTING OFF /REPORTING E', 
		@database_name=N'master', 
		@output_file_name=N'c:\Program Files\Microsoft SQL Server\MSSQL.1\MSSQL\LOG\OMC Load 1.txt', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [HEAT Email]    Script Date: 10/18/2018 2:03:01 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'HEAT Email', 
		@step_id=2, 
		@cmdexec_success_code=0, 
		@on_success_action=2, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'EXEC dbo.spJobNotification2005dbmail @jobname = ''OMC Load''', 
		@database_name=N'SAOnly', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'Continuous', 
		@enabled=1, 
		@freq_type=64, 
		@freq_interval=0, 
		@freq_subday_type=0, 
		@freq_subday_interval=0, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=0, 
		@active_start_date=20090416, 
		@active_end_date=99991231, 
		@active_start_time=0, 
		@active_end_time=235959
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:
GO

/****** Object:  Job [OMC Load Start]    Script Date: 10/18/2018 2:03:01 PM ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [[Uncategorized (Local)]]    Script Date: 10/18/2018 2:03:01 PM ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'[Uncategorized (Local)]' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'[Uncategorized (Local)]'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'OMC Load Start', 
		@enabled=1, 
		@notify_level_eventlog=0, 
		@notify_level_email=0, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'Starts OMC load jobs after midnight', 
		@category_name=N'[Uncategorized (Local)]', 
		@owner_login_name=N'sa', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Start jobs]    Script Date: 10/18/2018 2:03:01 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Start jobs', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=3, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'exec sp_start_job @job_name = ''OMC Load 1''
go
exec sp_start_job @job_name = ''OMC Load 2''
go
exec sp_start_job @job_name = ''OMC Load 3''
go
exec sp_start_job @job_name = ''OMC Load 4''
go
exec sp_start_job @job_name = ''OMC Load 5''
go
exec sp_start_job @job_name = ''OMC Load 6''
go
', 
		@database_name=N'msdb', 
		@output_file_name=N'c:\Program Files\Microsoft SQL Server\MSSQL.1\MSSQL\LOG\OMC Load Start.txt', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [HEAT Email]    Script Date: 10/18/2018 2:03:01 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'HEAT Email', 
		@step_id=2, 
		@cmdexec_success_code=0, 
		@on_success_action=2, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'EXEC dbo.spJobNotification2005dbmail @jobname = ''OMC Load start''', 
		@database_name=N'SAOnly', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'Midnight', 
		@enabled=1, 
		@freq_type=4, 
		@freq_interval=1, 
		@freq_subday_type=1, 
		@freq_subday_interval=0, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=0, 
		@active_start_date=20090518, 
		@active_end_date=99991231, 
		@active_start_time=100, 
		@active_end_time=235959
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:
GO

/****** Object:  Job [OMC Load stop]    Script Date: 10/18/2018 2:03:01 PM ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [[Uncategorized (Local)]]    Script Date: 10/18/2018 2:03:01 PM ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'[Uncategorized (Local)]' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'[Uncategorized (Local)]'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'OMC Load stop', 
		@enabled=1, 
		@notify_level_eventlog=0, 
		@notify_level_email=0, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'No description available.', 
		@category_name=N'[Uncategorized (Local)]', 
		@owner_login_name=N'sa', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Stop jobs]    Script Date: 10/18/2018 2:03:01 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Stop jobs', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=3, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'exec sp_stop_job @job_name = ''OMC Load 1''
go
exec sp_stop_job @job_name = ''OMC Load 2''
go
exec sp_stop_job @job_name = ''OMC Load 3''
go
exec sp_stop_job @job_name = ''OMC Load 4''
go
exec sp_stop_job @job_name = ''OMC Load 5''
go
exec sp_stop_job @job_name = ''OMC Load 6''
go', 
		@database_name=N'msdb', 
		@output_file_name=N'c:\Program Files\Microsoft SQL Server\MSSQL.1\MSSQL\LOG\OMC Load stop.txt', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [HEAT Email]    Script Date: 10/18/2018 2:03:01 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'HEAT Email', 
		@step_id=2, 
		@cmdexec_success_code=0, 
		@on_success_action=2, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'EXEC dbo.spJobNotification2005dbmail @jobname = ''OMC Load stop''', 
		@database_name=N'SAOnly', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'8PM Daily', 
		@enabled=1, 
		@freq_type=4, 
		@freq_interval=1, 
		@freq_subday_type=1, 
		@freq_subday_interval=0, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=0, 
		@active_start_date=20090518, 
		@active_end_date=99991231, 
		@active_start_time=180000, 
		@active_end_time=235959
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:
GO

